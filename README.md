# README #

Estudo para algoritmo que possa separar frases de  ementa de frases de preambulo.
Trata o presente notebook jupyter de estudo visando a verificar a viabilidade e assertividade de proposta de técnica de separação de textos registrados nos segmentos referentes a ementas.

O estudo se faz necessário dado que o parser empregado ( originário de : https://github.com/lexml/lexml-parser-projeto-lei) visa a tratar as normas legais, ao ser aplicado a atos infralegais ele apresenta erros, como o em questão, que diz respeito a identificar segmentos separados - ementa e preâmbulo - como segmentos únicos.

Esta esforço pretende verificar se as variáveis - verbos no gerúndio, ordem do período em relação ao início da norma (primeiro período da norma, segundo da norma, terceiro….), nome de órgão e nome de autoridade - se constituem o bastante para poder, através de técnica de regressão linear, prever o pertencimento das frases ( iniciando com letra maiúscula e terminando com ponto) dos atos infralegais.

Será aceito o índice de significância de 10% nesta ação. 


## O que há neste repositório  ###

* APES_Ementas_de_atos_que_contem_artigos.csv	 - arquivo com pré tratamento ,  a partir dos dados enviados pelo SERPRO. 
* DadosTratados.csv - dados estatiísticos gerados a partir de mineração de texto + técnicas de processamento de linguagem natural e classificação de amostra de frases
* Preambulo-Ementa normas infralegais.ipynb - caderno de jupyter com mais explicações , tratamento de dados e algoritmo de aprendizagem supervisionada utilizando-se de regressão logística
* finalized_model.sav - arquivo serializado do python para reuso do modelo de classificação de frases. 
 
Foram utilizadas 1000 frases classificadas  ( 70% treino , 30% teste), o modelo desenvolvido atingiu os seguintes índices : 


| Classe  | precisão| recall|
| --------|---------|-------|
| Ementa  | 0.98    | 0.98  |
| Preambu | 0.99    | 0.99  |



### Contribuição ###



* Jeferson Tadeu de Souza
* Projeto Codex


